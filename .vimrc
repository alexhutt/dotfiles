" Auto update files when modified
set autoread

" Highlight search results
set hlsearch

" Syntax highlighting
syntax enable

" Tab settings
set smarttab
set shiftwidth=4
set tabstop=4
set ai
set si
set wrap

" Line numbers
set number
